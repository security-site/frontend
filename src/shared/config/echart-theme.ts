const echart_theme = {
    'color': [
        '#1eb980',
        '#045d56',
        '#ffc107',
        '#eb5e4d',
        '#77cfe9',
        '#ef7e4f',
        '#b15dfd',
        '#ea7ccc'
    ],
    'backgroundColor': '#373740',
    'textStyle': {},
    'title': {
        'textStyle': {
            'color': '#eeeeee'
        },
        'subtextStyle': {
            'color': '#aaaaaa'
        }
    },
    'line': {
        'itemStyle': {
            'borderWidth': 1
        },
        'lineStyle': {
            'width': 2
        },
        'symbolSize': 4,
        'symbol': 'circle',
        'smooth': false
    },
    'radar': {
        'itemStyle': {
            'borderWidth': 1
        },
        'lineStyle': {
            'width': 2
        },
        'symbolSize': 4,
        'symbol': 'circle',
        'smooth': false
    },
    'bar': {
        'itemStyle': {
            'barBorderWidth': 0,
            'barBorderColor': '#cccccc'
        }
    },
    'pie': {
        'itemStyle': {
            'borderWidth': 0,
            'borderColor': '#cccccc'
        }
    },
    'scatter': {
        'itemStyle': {
            'borderWidth': 0,
            'borderColor': '#cccccc'
        }
    },
    'boxplot': {
        'itemStyle': {
            'borderWidth': 0,
            'borderColor': '#cccccc'
        }
    },
    'parallel': {
        'itemStyle': {
            'borderWidth': 0,
            'borderColor': '#cccccc'
        }
    },
    'sankey': {
        'itemStyle': {
            'borderWidth': 0,
            'borderColor': '#cccccc'
        }
    },
    'funnel': {
        'itemStyle': {
            'borderWidth': 0,
            'borderColor': '#cccccc'
        }
    },
    'gauge': {
        'itemStyle': {
            'borderWidth': 0,
            'borderColor': '#cccccc'
        }
    },
    'candlestick': {
        'itemStyle': {
            'color': '#eb5e4d',
            'color0': '#1eb980',
            'borderColor': '#eb5e4d',
            'borderColor0': '#1eb980',
            'borderWidth': '2'
        }
    },
    'graph': {
        'itemStyle': {
            'borderWidth': 0,
            'borderColor': '#cccccc'
        },
        'lineStyle': {
            'width': 1,
            'color': '#aaaaaa'
        },
        'symbolSize': 4,
        'symbol': 'circle',
        'smooth': false,
        'color': [
            '#1eb980',
            '#045d56',
            '#ffc107',
            '#eb5e4d',
            '#77cfe9',
            '#ef7e4f',
            '#b15dfd',
            '#ea7ccc'
        ],
        'label': {
            'color': '#eeeeee'
        }
    },
    'map': {
        'itemStyle': {
            'normal': {
                'areaColor': '#eee',
                'borderColor': '#444',
                'borderWidth': 0.5
            },
            'emphasis': {
                'areaColor': 'rgba(255,215,0,0.8)',
                'borderColor': '#444',
                'borderWidth': 1
            }
        },
        'label': {
            'normal': {
                'textStyle': {
                    'color': '#000'
                }
            },
            'emphasis': {
                'textStyle': {
                    'color': 'rgb(100,0,0)'
                }
            }
        }
    },
    'geo': {
        'itemStyle': {
            'normal': {
                'areaColor': '#eee',
                'borderColor': '#444',
                'borderWidth': 0.5
            },
            'emphasis': {
                'areaColor': 'rgba(255,215,0,0.8)',
                'borderColor': '#444',
                'borderWidth': 1
            }
        },
        'label': {
            'normal': {
                'textStyle': {
                    'color': '#000'
                }
            },
            'emphasis': {
                'textStyle': {
                    'color': 'rgb(100,0,0)'
                }
            }
        }
    },
    'categoryAxis': {
        'axisLine': {
            'show': true,
            'lineStyle': {
                'color': '#eeeeee'
            }
        },
        'axisTick': {
            'show': true,
            'lineStyle': {
                'color': '#eeeeee'
            }
        },
        'axisLabel': {
            'show': true,
            'textStyle': {
                'color': '#eeeeee'
            }
        },
        'splitLine': {
            'show': true,
            'lineStyle': {
                'color': [
                    '#aaaaaa'
                ]
            }
        },
        'splitArea': {
            'show': false,
            'areaStyle': {
                'color': [
                    '#eeeeee'
                ]
            }
        }
    },
    'valueAxis': {
        'axisLine': {
            'show': true,
            'lineStyle': {
                'color': '#eeeeee'
            }
        },
        'axisTick': {
            'show': true,
            'lineStyle': {
                'color': '#eeeeee'
            }
        },
        'axisLabel': {
            'show': true,
            'textStyle': {
                'color': '#eeeeee'
            }
        },
        'splitLine': {
            'show': true,
            'lineStyle': {
                'color': [
                    '#aaaaaa'
                ]
            }
        },
        'splitArea': {
            'show': false,
            'areaStyle': {
                'color': [
                    '#eeeeee'
                ]
            }
        }
    },
    'logAxis': {
        'axisLine': {
            'show': true,
            'lineStyle': {
                'color': '#eeeeee'
            }
        },
        'axisTick': {
            'show': true,
            'lineStyle': {
                'color': '#eeeeee'
            }
        },
        'axisLabel': {
            'show': true,
            'textStyle': {
                'color': '#eeeeee'
            }
        },
        'splitLine': {
            'show': true,
            'lineStyle': {
                'color': [
                    '#aaaaaa'
                ]
            }
        },
        'splitArea': {
            'show': false,
            'areaStyle': {
                'color': [
                    '#eeeeee'
                ]
            }
        }
    },
    'timeAxis': {
        'axisLine': {
            'show': true,
            'lineStyle': {
                'color': '#eeeeee'
            }
        },
        'axisTick': {
            'show': true,
            'lineStyle': {
                'color': '#eeeeee'
            }
        },
        'axisLabel': {
            'show': true,
            'textStyle': {
                'color': '#eeeeee'
            }
        },
        'splitLine': {
            'show': true,
            'lineStyle': {
                'color': [
                    '#aaaaaa'
                ]
            }
        },
        'splitArea': {
            'show': false,
            'areaStyle': {
                'color': [
                    '#eeeeee'
                ]
            }
        }
    },
    'toolbox': {
        'iconStyle': {
            'normal': {
                'borderColor': '#999999'
            },
            'emphasis': {
                'borderColor': '#666666'
            }
        }
    },
    'legend': {
        'textStyle': {
            'color': '#eeeeee'
        }
    },
    'tooltip': {
        'axisPointer': {
            'lineStyle': {
                'color': '#eeeeee',
                'width': '1'
            },
            'crossStyle': {
                'color': '#eeeeee',
                'width': '1'
            }
        }
    },
    'timeline': {
        'lineStyle': {
            'color': '#eeeeee',
            'width': 1
        },
        'itemStyle': {
            'normal': {
                'color': '#dd6b66',
                'borderWidth': 1
            },
            'emphasis': {
                'color': '#a9334c'
            }
        },
        'controlStyle': {
            'normal': {
                'color': '#eeeeee',
                'borderColor': '#eeeeee',
                'borderWidth': 0.5
            },
            'emphasis': {
                'color': '#eeeeee',
                'borderColor': '#eeeeee',
                'borderWidth': 0.5
            }
        },
        'checkpointStyle': {
            'color': '#e43c59',
            'borderColor': '#c23531'
        },
        'label': {
            'normal': {
                'textStyle': {
                    'color': '#eeeeee'
                }
            },
            'emphasis': {
                'textStyle': {
                    'color': '#eeeeee'
                }
            }
        }
    },
    'visualMap': {
        'color': [
            '#bf444c',
            '#d88273',
            '#f6efa6'
        ]
    },
    'dataZoom': {
        'backgroundColor': 'rgba(47,69,84,0)',
        'dataBackgroundColor': 'rgba(255,255,255,0.3)',
        'fillerColor': 'rgba(167,183,204,0.4)',
        'handleColor': '#a7b7cc',
        'handleSize': '100%',
        'textStyle': {
            'color': '#eeeeee'
        }
    },
    'markPoint': {
        'label': {
            'color': '#eeeeee'
        },
        'emphasis': {
            'label': {
                'color': '#eeeeee'
            }
        }
    }
}
export default echart_theme
