export enum Bank {
    SBER = 'SBER',
}

export enum BankImport {
    SBER_ONLINE = 'SBER_ONLINE',
    TINKOFF_ONLINE = 'TINKOFF_ONLINE',
}
