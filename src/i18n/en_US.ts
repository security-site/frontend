const en_US = {
  'failed': 'Action failed',
  'success': 'Action was successful',
  'messages': 'hello i18n !!',
  'value': 'Value',
  'ChangeDay': 'ChangeDay',
  'Money': 'Money',
  'ConnectionUnexpectedClosed': 'Connection unexpected closed',
  'portfolio': 'Портфолио',
}

export default en_US
