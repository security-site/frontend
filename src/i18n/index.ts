import en_US from './en_US'
import {createI18n, IntlNumberFormats, LocaleMessages, VueMessageType} from 'vue-i18n'
import ru_RU from 'src/i18n/ru_RU'
import {DateTimeFormats as IntlDateTimeFormats} from '@intlify/core-base'

const messages: LocaleMessages<VueMessageType> = {
  'en-US': en_US,
  'ru-RU': ru_RU,
}


const numberFormats: IntlNumberFormats = {
  'USD': {
    currency: {
      style: 'currency',
      currency: 'USD',
      maximumFractionDigits: 8,
    },
    price: {
      style: 'currency',
      currency: 'RUB',
      currencyDisplay: 'symbol',
      maximumFractionDigits: 8,
    },
    percent: {
      style: 'percent',
    },
  },
  'RUB': {
    currency: {
      style: 'currency',
      currency: 'RUB',
      currencyDisplay: 'symbol',
      maximumFractionDigits: 2,
    },
    price: {
      style: 'currency',
      currency: 'RUB',
      currencyDisplay: 'symbol',
      maximumFractionDigits: 8,
    },
    percent: {
      style: 'percent',
    },
  },
  'EUR': {
    currency: {
      style: 'currency',
      currency: 'EUR',
      currencyDisplay: 'symbol',
    },
    percent: {
      style: 'percent',
    },
  },
}

const datetimeFormats: IntlDateTimeFormats = {
  'en-US': {
    short: {
      year: 'numeric', month: 'numeric', day: 'numeric',
    },
    long: {
      year: 'numeric', month: 'short', day: 'numeric',
      weekday: 'short', hour: 'numeric', minute: 'numeric',
    },
  },
  'ru-RU': {
    short: {
      year: 'numeric', month: 'numeric', day: 'numeric',
    },
    long: {
      year: 'numeric', month: 'short', day: 'numeric',
      weekday: 'narrow', hour: 'numeric', minute: 'numeric',
    },
  },
}

const i18n = createI18n({
  legacy: false,
  globalInjection: true,
  locale: 'ru-RU',
  messages,
  datetimeFormats,
  numberFormats
})

export default i18n
