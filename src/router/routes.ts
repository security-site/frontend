import {RouteRecordRaw} from 'vue-router'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'main',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        name: 'page_home',
        path: '/home',
        component: () => import('pages/PageHome.vue'),
      },
      {
        name: 'page_trades',
        path: '/trades',
        component: () => import('pages/PageTrades.vue'),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
]

export default routes
