import {createModule} from 'vuexok'
import {TradeDTO} from 'src/api/portfolio/types/tradeDTO'
import {PortfolioApi} from 'src/api/portfolio/portfolio-api'
import {StockPortfolioDTO} from 'src/api/portfolio/stockPortfolioDTO'

export const tradesStore = createModule(
  'tradesStore',
  {
    namespaced: true,
    state: {
      lastDate: undefined,
      portfolio: new Array<StockPortfolioDTO>(),
      trades: new Array<TradeDTO>(),
    },
    mutations: {
      setPortfolio(state, portfolio: Array<StockPortfolioDTO>) {
        state.portfolio = portfolio
      },
      setTrades(state, trades: Array<TradeDTO>) {
        state.trades = trades
      },
    },
    actions: {
      async loadPortfolio() {
        const portfolio = await PortfolioApi.getPortfolio()
        tradesStore.mutations.setPortfolio(portfolio)
      },
      async loadTrades(state) {
        const tradeDTOS = await PortfolioApi.getTrades(state.state.lastDate)
        tradesStore.mutations.setTrades(tradeDTOS)
      },
    },
    modules: {},
  })
