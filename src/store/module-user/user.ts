export default class User {
  constructor(public username: string,
              public email: string,
              public firstname: string,
              public lastname: string,
              public roles: string[],
              public lastLogin: string,
  ) {
  }
}
