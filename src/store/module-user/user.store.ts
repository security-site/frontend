import {createModule} from 'vuexok'
import Keycloak from 'keycloak-js'
import User from 'src/store/module-user/user'

export enum Currency {
  RUB = 'RUB',
  USD = 'USD',
  EUR = 'EUR'
}

const guest = new User('Гость', '', 'Гость', '', [], '')

export const userStore = createModule(
  'userStore',
  {
    namespaced: true,
    state: {
      profile: guest,
      token: '',
      currency: Currency.RUB,
      bank: '',
      bankImport: '',
    },
    mutations: {
      setBankImport(state, bankImport: string) {
        state.bankImport = bankImport
      },
      setUser(state, user: User) {
        state.profile = user
      },
      changeToken(state, token: string) {
        state.token = token
      },
      changeCurrency(state, currency: Currency) {
        state.currency = currency
      },
    },
    actions: {
      async loadProfile(state, keycloak: Keycloak.KeycloakInstance): Promise<void> {
        const profile = await keycloak.loadUserProfile()
        const userDetails: User = new User(profile.username ?? '',
          profile.email ?? '',
          profile.firstName ?? '',
          profile.lastName ?? '',
          keycloak.realmAccess?.roles ?? [],
          '')
        userStore.mutations.setUser(userDetails)
      },
    },
    modules: {},
  })
