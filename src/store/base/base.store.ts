import {createModule} from 'vuexok'

export const coreModule = createModule(
  'coreStore',
  {
    namespaced: true,
    state: {
      drawer: true,
    },
    mutations: {
      setDrawer(state, drawer: boolean) {
        state.drawer = drawer
      },
    },
    actions: {},
    modules: {},
  })
