import {boot} from 'quasar/wrappers'
import {userStore} from 'src/store/module-user/user.store'
import {coreModule} from 'src/store/base/base.store'
import {tradesStore} from 'src/store/portfolio/tradesStore'
import 'reflect-metadata'

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(({store}) => {
  userStore.register(store)
  coreModule.register(store)
  tradesStore.register(store)
})
