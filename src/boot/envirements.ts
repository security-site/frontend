
const base_url = process.env.VUE_APP_API_BASE_URL
const keycloakURL = process.env.VUE_APP_KEYCLOAK_URL
const keycloakRealm = process.env.VUE_APP_KEYCLOAK_REALM
const keycloakClientId = process.env.VUE_APP_KEYCLOAK_CLIENT_ID

export { keycloakURL, keycloakRealm, keycloakClientId, base_url}
