import {boot} from 'quasar/wrappers'
import Keycloak from 'keycloak-js'
import {keycloakClientId, keycloakRealm, keycloakURL} from 'boot/envirements'
import {userStore} from 'src/store/module-user/user.store'

// Initialisation options
const kc_init_options: Keycloak.KeycloakInitOptions = {
  adapter: 'default',
  onLoad: 'login-required',
  checkLoginIframe: false,
}

const kc_config: Keycloak.KeycloakConfig = {
  url: keycloakURL || 'VUE_APP_KEYCLOAK_URL is undefined',
  realm: keycloakRealm || 'VUE_APP_KEYCLOAK_REALM is undefined',
  clientId: keycloakClientId || 'VUE_APP_KEYCLOAK_CLIENT_ID is undefined',
}

// Keycloak instance
const keycloak: Keycloak.KeycloakInstance = Keycloak(kc_config)

keycloak.onAuthSuccess = () => {
  userStore.mutations.changeToken(keycloak.token ?? '')
  userStore.actions.loadProfile(keycloak)
    .then(() => console.log('Профиль пользователя сохранен'))
    .catch(reason => {
      console.log('Ошибка загрузки профиля пользователя:')
      console.error(JSON.stringify(reason))
    })
}


const TOKEN_MIN_VALIDITY_SECONDS = 70

export async function updateToken(): Promise<string | undefined> {
  await keycloak.updateToken(TOKEN_MIN_VALIDITY_SECONDS)
  return keycloak.token
}

window.onfocus = () => {
  updateToken().then((jwt) => {
    if (jwt != null) {
      userStore.mutations.changeToken(jwt)
    }
  }).catch(() => {
    console.log('Failed to refresh token')
  })
}


// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
// noinspection JSUnusedGlobalSymbols
export default boot(async ({app, router}) => {
  return new Promise(resolve => {
    keycloak.init(kc_init_options)
      .then(async (authenticated) => {
        if (authenticated) {
          console.log('Authenticated')
          await updateToken()
          resolve()
        } else {
          console.log('Not authenticated')
        }
      }).catch((error) => {
      console.log('Authentication failure', error)

    })
    app.config.globalProperties.$keycloak = keycloak
    void router.push({name: 'page_home'})
  })
})

