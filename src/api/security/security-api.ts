import {AxiosResponse} from 'axios'
import {CurrencyRateDTO} from 'src/api/security/types/currencyRateDTO'
import {assertParamExists} from 'src/api/common'
import {plainToClass} from 'class-transformer'
import {api} from 'boot/axios'
import {SecurityDTO} from 'src/api/security/types/securityDTO'
import {SecurityPriceDTO} from 'src/api/security/types/securityPriceDTO'

/**
 * SecurityControllerApi - object-oriented interface
 * @export
 * @class SecurityControllerApi
 */
export class SecurityControllerApi {
    /**
     * Получить коэфиценты для валютной пары.
     * @param {string} from
     * @param {string} to
     * @param {string} [fromDate]
     * @throws {RequiredError}
     * @memberof SecurityControllerApi
     */
    public getCurrencyRate(from: string, to: string, fromDate?: Date): Promise<CurrencyRateDTO> {
        assertParamExists('getCurrencyRate', 'from', from)
        assertParamExists('getCurrencyRate', 'to', to)
        return api.get('/api/v1/security-info/currency-rate', {params: {from: from, to: to, fromDate: fromDate}})
            .then((request: AxiosResponse<CurrencyRateDTO>) => plainToClass(CurrencyRateDTO, request.data))
    }

    /**
     * Получить информацию по бумаге
     * @param {string} ticker
     * @param {'ALL' | 'PRICE_ONLY'} type
     * @param {string} [exchange]
     * @param {string} [from]
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof SecurityControllerApi
     */
    public getSecurity(ticker: string, type: 'ALL' | 'PRICE_ONLY', exchange?: string, from?: string): Promise<SecurityDTO> {
        assertParamExists('getSecurity', 'type', type)
        return api.get('/api/v1/security-info/security', {
            params: {
                ticker: ticker,
                type: type,
                exchange: exchange,
                from: from
            }
        })
            .then((request: AxiosResponse<SecurityDTO>) => plainToClass(SecurityDTO, request.data))
    }

    /**
     * Получить последнюю цену по бумаге по бумаге. (Указание идинтификатора биржы обязательно (Московская: MISX) )
     * @param {string} ticker
     * @param {string} exchange
     * @param {'ALL' | 'PRICE_ONLY'} type
     * @param {string} [from]
     * @param {*} [options] Override http request option.
     * @throws {RequiredError}
     * @memberof SecurityControllerApi
     */
    public getSecurityPrice(ticker: string, exchange: string, type: 'ALL' | 'PRICE_ONLY', from?: string): Promise<SecurityPriceDTO> {
        assertParamExists('getSecurityPrice', 'ticker', ticker)
        assertParamExists('getSecurityPrice', 'exchange', exchange)
        assertParamExists('getSecurityPrice', 'type', type)
        return api.get('/api/v1/security-info/security-price', {
            params: {
                ticker: ticker,
                type: type,
                exchange: exchange,
                from: from
            }
        })
            .then((request: AxiosResponse<SecurityPriceDTO>) => plainToClass(SecurityPriceDTO, request.data))
    }
}
