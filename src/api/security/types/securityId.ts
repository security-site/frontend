/**
 *
 * @export
 * @class SecurityId
 */
export class SecurityId {
    /**
     *
     * @type {string}
     * @memberof SecurityId
     */
    'ticker'?: string;
    /**
     *
     * @type {string}
     * @memberof SecurityId
     */
    'exchange'?: string;
}