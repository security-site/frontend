/**
 *
 * @export
 * @class SecurityPriceDTO
 */
export class SecurityPriceDTO {
    /**
     *
     * @type {string}
     * @memberof SecurityPriceDTO
     */
    'ticker'?: string;
    /**
     *
     * @type {string}
     * @memberof SecurityPriceDTO
     */
    'exchange'?: string;
    /**
     *
     * @type {string}
     * @memberof SecurityPriceDTO
     */
    'time'?: string;
    /**
     *
     * @type {number}
     * @memberof SecurityPriceDTO
     */
    'max'?: number;
    /**
     *
     * @type {number}
     * @memberof SecurityPriceDTO
     */
    'min'?: number;
    /**
     *
     * @type {number}
     * @memberof SecurityPriceDTO
     */
    'open'?: number;
    /**
     *
     * @type {number}
     * @memberof SecurityPriceDTO
     */
    'last'?: number;
    /**
     *
     * @type {string}
     * @memberof SecurityPriceDTO
     */
    'currency'?: string;
    /**
     *
     * @type {number}
     * @memberof SecurityPriceDTO
     */
    'value'?: number;
    /**
     *
     * @type {number}
     * @memberof SecurityPriceDTO
     */
    'numTrades'?: number;
}