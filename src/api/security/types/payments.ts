/**
 *
 * @export
 * @interface AbstractPaymentDTO
 */
import {MoneyDTO} from 'src/api/security/types/moneyDTO'


export interface AbstractPaymentDTO {
  /**
   *
   * @type {string}
   * @memberof AbstractPaymentDTO
   */
  'date'?: string;
  /**
   *
   * @type {MoneyDTO}
   * @memberof AbstractPaymentDTO
   */
  'money'?: MoneyDTO;
  /**
   *
   * @type {string}
   * @memberof AbstractPaymentDTO
   */
  'type': string;
}

/**
 *
 * @export
 * @interface AmortizationDTO
 */
export interface AmortizationDTO extends AbstractPaymentDTO {
  /**
   *
   * @type {MoneyDTO}
   * @memberof AmortizationDTO
   */
  'faceValue'?: MoneyDTO;
  /**
   *
   * @type {number}
   * @memberof AmortizationDTO
   */
  'valuePercent'?: number;
  /**
   *
   * @type {MoneyDTO}
   * @memberof AmortizationDTO
   */
  'valueRub'?: MoneyDTO;
}

/**
 *
 * @export
 * @interface AmortizationDTOAllOf
 */
export interface AmortizationDTOAllOf {
  /**
   *
   * @type {MoneyDTO}
   * @memberof AmortizationDTOAllOf
   */
  'faceValue'?: MoneyDTO;
  /**
   *
   * @type {number}
   * @memberof AmortizationDTOAllOf
   */
  'valuePercent'?: number;
  /**
   *
   * @type {MoneyDTO}
   * @memberof AmortizationDTOAllOf
   */
  'valueRub'?: MoneyDTO;
}

/**
 *
 * @export
 * @interface CouponDTO
 */
export interface CouponDTO extends AbstractPaymentDTO {
  /**
   *
   * @type {MoneyDTO}
   * @memberof CouponDTO
   */
  'faceValue'?: MoneyDTO;
  /**
   *
   * @type {number}
   * @memberof CouponDTO
   */
  'valuePercent'?: number;
  /**
   *
   * @type {MoneyDTO}
   * @memberof CouponDTO
   */
  'valueRub'?: MoneyDTO;
}

/**
 *
 * @export
 * @interface DividendDTO
 */
export class DividendDTO implements AbstractPaymentDTO {
  constructor(public date: string,
              public money: MoneyDTO,
              public type: string) {
  }
}
