import {CurrencyPairDTO} from 'src/api/security/types/currencyPairDTO'
import {RateHistoryDTO} from 'src/api/security/types/rateHistoryDTO'

/**
 *
 * @export
 * @class CurrencyRateDTO
 */
export class CurrencyRateDTO {
    /**
     *
     * @type {CurrencyPairDTO}
     * @memberof CurrencyRateDTO
     */
    'currency'?: CurrencyPairDTO;
    /**
     *
     * @type {number}
     * @memberof CurrencyRateDTO
     */
    'last'?: number;
    /**
     *
     * @type {Array<RateHistoryDTO>}
     * @memberof CurrencyRateDTO
     */
    'history'?: Array<RateHistoryDTO>;
}
