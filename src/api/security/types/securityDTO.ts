import {MoneyDTO} from 'src/api/security/types/moneyDTO'
import {Money} from 'src/api/security/types/money'
import {SectorDTO} from 'src/api/security/types/sectorDTO'
import {HistoryDTO} from 'src/api/security/types/historyDTO'
import {AmortizationDTO, CouponDTO, DividendDTO} from 'src/api/security/types/payments'
import {SecurityId} from 'src/api/security/types/securityId'
import {SecurityTypeDTO} from 'src/api/security/types/securityTypeDTO'

/**
 *
 * @export
 * @class SecurityDTO
 */
export class SecurityDTO {
    /**
     *
     * @type {string}
     * @memberof SecurityDTO
     */
    'ticker'?: string;
    /**
     *
     * @type {string}
     * @memberof SecurityDTO
     */
    'exchange'?: string;
    /**
     *
     * @type {string}
     * @memberof SecurityDTO
     */
    'isin'?: string;
    /**
     *
     * @type {string}
     * @memberof SecurityDTO
     */
    'shortName'?: string;
    /**
     *
     * @type {string}
     * @memberof SecurityDTO
     */
    'fullName'?: string;
    /**
     *
     * @type {string}
     * @memberof SecurityDTO
     */
    'shortLatName'?: string;
    /**
     *
     * @type {string}
     * @memberof SecurityDTO
     */
    'fullLatName'?: string;
    /**
     *
     * @type {MoneyDTO}
     * @memberof SecurityDTO
     */
    'faceValue'?: MoneyDTO;
    /**
     *
     * @type {number}
     * @memberof SecurityDTO
     */
    'lotSize'?: number;
    /**
     *
     * @type {string}
     * @memberof SecurityDTO
     */
    'issueDate'?: string;
    /**
     *
     * @type {number}
     * @memberof SecurityDTO
     */
    'issueSize'?: number;
    /**
     *
     * @type {Money}
     * @memberof SecurityDTO
     */
    'issueCapitalization'?: Money;
    /**
     *
     * @type {SecurityTypeDTO}
     * @memberof SecurityDTO
     */
    'type'?: SecurityTypeDTO;
    /**
     *
     * @type {SectorDTO}
     * @memberof SecurityDTO
     */
    'sector'?: SectorDTO;
    /**
     *
     * @type {boolean}
     * @memberof SecurityDTO
     */
    'qualifiedInvestors'?: boolean;
    /**
     *
     * @type {string}
     * @memberof SecurityDTO
     */
    'site'?: string;
    /**
     *
     * @type {Array<HistoryDTO>}
     * @memberof SecurityDTO
     */
    'history'?: Array<HistoryDTO>;
    /**
     *
     * @type {Array<AmortizationDTO | CouponDTO | DividendDTO>}
     * @memberof SecurityDTO
     */
    'payments'?: Array<AmortizationDTO | CouponDTO | DividendDTO>;
    /**
     *
     * @type {string}
     * @memberof SecurityDTO
     */
    'lastUpdate'?: string;
    /**
     *
     * @type {SecurityId}
     * @memberof SecurityDTO
     */
    'id'?: SecurityId;
}
