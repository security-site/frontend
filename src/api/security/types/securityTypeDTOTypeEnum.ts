/**
 * @export
 * @enum {string}
 */
export enum SecurityTypeDTOTypeEnum {
    Unrecognized = 'UNRECOGNIZED',
    Index = 'INDEX',
    StockShares = 'STOCK_SHARES',
    StockEtf = 'STOCK_ETF',
    StockBonds = 'STOCK_BONDS',
    Currency = 'CURRENCY',
    FuturesForts = 'FUTURES_FORTS',
    FuturesOptions = 'FUTURES_OPTIONS',
    CurrencyMetal = 'CURRENCY_METAL',
    Other = 'OTHER'
}