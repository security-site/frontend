import {SectorDTOTypeEnum} from 'src/api/security/types/sectorDTOTypeEnum'

/**
 *
 * @export
 * @class SectorDTO
 */
export class SectorDTO {
    /**
     *
     * @type {string}
     * @memberof SectorDTO
     */
    'type'?: SectorDTOTypeEnum;
    /**
     *
     * @type {string}
     * @memberof SectorDTO
     */
    'raw'?: string;
}
