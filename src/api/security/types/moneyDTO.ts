/**
 *
 * @export
 * @class MoneyDTO
 */
export class MoneyDTO {
    /**
     *
     * @type {number}
     * @memberof MoneyDTO
     */
    'amount'?: number;
    /**
     *
     * @type {string}
     * @memberof MoneyDTO
     */
    'currency'?: string;
}