/**
 * @export
 * @enum {string}
 */
export enum SecurityTypeDTOSubTypeEnum {
    Unrecognized = 'UNRECOGNIZED',
    CommonShare = 'COMMON_SHARE',
    PreferredShare = 'PREFERRED_SHARE',
    DepositaryReceipt = 'DEPOSITARY_RECEIPT',
    OfzBond = 'OFZ_BOND',
    CbBond = 'CB_BOND',
    SubFederalBond = 'SUB_FEDERAL_BOND',
    MunicipalBond = 'MUNICIPAL_BOND',
    CorporateBond = 'CORPORATE_BOND',
    ExchangeBond = 'EXCHANGE_BOND',
    IfiBond = 'IFI_BOND',
    EuroBond = 'EURO_BOND',
    PublicPpif = 'PUBLIC_PPIF',
    IntervalPpif = 'INTERVAL_PPIF',
    RtsIndex = 'RTS_INDEX',
    PrivatePpif = 'PRIVATE_PPIF',
    StockMortgage = 'STOCK_MORTGAGE',
    EtfPpif = 'ETF_PPIF',
    StockIndex = 'STOCK_INDEX',
    ExchangePpif = 'EXCHANGE_PPIF',
    StockDeposit = 'STOCK_DEPOSIT',
    NonExchangeBond = 'NON_EXCHANGE_BOND',
    StateBond = 'STATE_BOND',
    CurrencyIndex = 'CURRENCY_INDEX',
    CurrencySub = 'CURRENCY_SUB',
    Metal = 'METAL',
    OtherSub = 'OTHER_SUB'
}