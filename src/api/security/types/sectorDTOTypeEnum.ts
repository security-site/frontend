/**
 * @export
 * @enum {string}
 */
export enum SectorDTOTypeEnum {
    Unrecognized = 'UNRECOGNIZED',
    Transport = 'TRANSPORT',
    Logistics = 'LOGISTICS',
    Chemistry = 'CHEMISTRY',
    Holdings = 'HOLDINGS',
    RetailTrade = 'RETAIL_TRADE',
    Energy = 'ENERGY',
    EnergyResources = 'ENERGY_RESOURCES',
    FinanceBanking = 'FINANCE_BANKING',
    Industry = 'INDUSTRY',
    MetalsMining = 'METALS_MINING',
    RealEstate = 'REAL_ESTATE',
    Telecommunications = 'TELECOMMUNICATIONS',
    Technology = 'TECHNOLOGY',
    Agriculture = 'AGRICULTURE',
    Healthcare = 'HEALTHCARE',
    Funds = 'FUNDS',
    Construction = 'CONSTRUCTION'
}