export class CurrencyPairDTO {
    /**
     *
     * @type {string}
     * @memberof CurrencyPairDTO
     */
    'from'?: string;
    /**
     *
     * @type {string}
     * @memberof CurrencyPairDTO
     */
    'to'?: string;
}