/**
 *
 * @export
 * @class RateHistoryDTO
 */
export class RateHistoryDTO {
    /**
     *
     * @type {string}
     * @memberof RateHistoryDTO
     */
    'date'?: string;
    /**
     *
     * @type {number}
     * @memberof RateHistoryDTO
     */
    'rate'?: number;
}