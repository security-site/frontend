import {SecurityTypeDTOTypeEnum} from 'src/api/security/types/securityTypeDTOTypeEnum'
import {SecurityTypeDTOSubTypeEnum} from 'src/api/security/types/securityTypeDTOSubTypeEnum'

/**
 *
 * @export
 * @class SecurityTypeDTO
 */
export class SecurityTypeDTO {
    /**
     *
     * @type {string}
     * @memberof SecurityTypeDTO
     */
    'type'?: SecurityTypeDTOTypeEnum;
    /**
     *
     * @type {string}
     * @memberof SecurityTypeDTO
     */
    'subType'?: SecurityTypeDTOSubTypeEnum;
    /**
     *
     * @type {string}
     * @memberof SecurityTypeDTO
     */
    'raw'?: string;
}
