import {MoneyDTO} from 'src/api/security/types/moneyDTO'
import {Type} from 'class-transformer'

/**
 *
 * @export
 * @class HistoryDTO
 */
export class HistoryDTO {

  id?: string

  /**
   *
   * @type {string}
   * @memberof HistoryDTO
   */
  @Type(() => Date)
  'date'?: Date
  /**
   *
   * @type {number}
   * @memberof HistoryDTO
   */
  'price'?: number
  /**
   *
   * @type {number}
   * @memberof HistoryDTO
   */
  'max'?: number
  /**
   *
   * @type {number}
   * @memberof HistoryDTO
   */
  'min'?: number
  /**
   *
   * @type {number}
   * @memberof HistoryDTO
   */
  'open'?: number
  /**
   *
   * @type {number}
   * @memberof HistoryDTO
   */
  'close'?: number
  /**
   *
   * @type {string}
   * @memberof HistoryDTO
   */
  'currency'?: string
  /**
   *
   * @type {MoneyDTO}
   * @memberof HistoryDTO
   */
  @Type(() => MoneyDTO)
  'faceValue'?: MoneyDTO
  /**
   *
   * @type {number}
   * @memberof HistoryDTO
   */
  'value'?: number
  /**
   *
   * @type {number}
   * @memberof HistoryDTO
   */
  'numTrades'?: number
}
