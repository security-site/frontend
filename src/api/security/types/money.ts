/**
 *
 * @export
 * @class Money
 */
export class Money {
  /**
   *
   * @type {number}
   * @memberof MonetaryAmount
   */
  'amount': number
  /**
   *
   * @type {string}
   * @memberof MonetaryAmount
   */
  'currency': string

  compare(other: Money) {
    if (this.amount == other.amount) {
      if (this.currency === other.currency)
        return 0
      else if (this.currency > other.currency)
        return 1
      else
        return -1
    } else if (this.amount > other.amount) {
      return 1
    } else
      return -1
  }
}
