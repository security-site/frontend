export class StockPortfolioDTO {
  'ticker': string
  'exchange'?: string
  'buyQuantity': number
  'saleQuantity': number
  'averagePrice': number
  'buySumPrice': number
  'saleSumPrice': number
  'priceCurrency': number
  'buyFee': number
  'saleFee': number
  'feeCurrency': number
  'dividend': number
  'dividendCurrency': number
  'firstTransaction': string
  'lastTransaction': string

  get sum(): number {
    return this.buySumPrice - this.saleSumPrice
  }
}

