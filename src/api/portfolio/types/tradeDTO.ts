import {Money} from 'src/api/security/types/money'
import {Type} from 'class-transformer'
import {TradeDTOOperationEnum} from 'src/api/portfolio/tradeDTOOperationEnum'

export class TradeDTO {
  'id'?: string
  @Type(() => Date)
  'date'?: Date
  @Type(() => Date)
  settlementDate?: Date
  'ticker'?: string
  'exchange'?: string
  'isin'?: string
  'operation'?: TradeDTOOperationEnum

  @Type(() => Money)
  'price'?: Money

  @Type(() => Money)
  'sum': Money
  'quantity'?: number
  'totalQuantity'?: number

  @Type(() => Money)
  'nkd'?: Money

  @Type(() => Money)
  'fee'?: Money
  'fileId'?: number
  'note'?: string

  public isMoney() {
    return this.ticker?.charAt(0) === '$'
  }
}
