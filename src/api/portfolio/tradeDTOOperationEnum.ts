/**
 * @export
 * @enum {string}
 */
export enum TradeDTOOperationEnum {
    Buy = 'BUY',
    Sale = 'SALE'
}
