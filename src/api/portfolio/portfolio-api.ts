import {AxiosRequestConfig, AxiosResponse} from 'axios'
import {assertParamExists} from 'src/api/common'
import {TradeDTO} from 'src/api/portfolio/types/tradeDTO'
import {plainToInstance} from 'class-transformer'
import {api} from 'boot/axios'
import {StockPortfolioDTO} from 'src/api/portfolio/stockPortfolioDTO'


export class PortfolioApi {

  public static async getPortfolio(): Promise<Array<StockPortfolioDTO>> {
    console.log('PortfolioApi.getPortfolio')
    return api.get('/api/v1/portfolio-info/portfolio')
      .then((request: AxiosResponse<StockPortfolioDTO[]>) => plainToInstance<StockPortfolioDTO, StockPortfolioDTO>(StockPortfolioDTO, request.data))
  }

  /**
   *
   * @param {string} [from] С какой даты возвращать значения
   * @throws {RequiredError}
   */
  public static async getTrades(from?: Date): Promise<Array<TradeDTO>> {
    return api.get('/api/v1/portfolio-info/trades',
      {
        params: {
          from: from?.toISOString().substr(0, 10),
        },
      })

      .then((request: AxiosResponse<TradeDTO[]>) => plainToInstance(TradeDTO, request.data))
  }

  /**
   *
   * @param {any} file
   * @param {string} bankImport
   * @param {*} [options] Override http request option.
   * @throws {RequiredError}
   * @memberof PortfolioControllerApi
   */
  public static async uploadFile(file: File, bankImport: string, options?: AxiosRequestConfig): Promise<number> {
    assertParamExists('uploadFile', 'file', file)
    assertParamExists('uploadFile', 'bankImport', bankImport)
    const formData = new FormData()
    formData.append('file', file)
    formData.append('bankImport', bankImport)
    return api.post('/api/v1/portfolio-info/upload-file', formData, options)
      .then((request: AxiosResponse<number>) => request.data)

  }
}

